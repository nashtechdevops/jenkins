# **Setup Jenkins pineline to build docker image**

# Install docker as administrator3

> `choco install Docker`

# Start docker desktop

![picture](images/docker.png)

# build jenkins images that installed docker

> `docker build -t jenkins-docker ./jenkins-docker`

# Start jenkins

> `docker run -d --name jenkins -v /var/run/docker.sock:/var/run/docker.sock -p 8080:8080 -p 50000:50000 jenkins-docker`

# Access Jenkins URL

> `http://localhost:8080`

# Show jenkins password admin to login jenkins

> `docker exec -it jenkins cat /var/jenkins_home/secrets/initialAdminPassword`

# Choose "Install suggested plugins"

![picture](images/jenkins1.png)

# Create first admin user

![picture](images/jenkins2.png)

# Access Plugins Manager

> `http://localhost:8080/pluginManager/available`

# Install Plugins Manager

> install plugin : 
>   - Amazon ECR
>   - Bitbucket
>   - Docker

![picture](images/plugin.png)

# Create the ECR Repository

> 1. Log in to your AWS Console
> 2. Open the EC2 Container Registry service.
> 3. Click the Create repository button in the Repositories tab.
> 4. Give a name to your repository. For the demo, I’m using "username". Then, click the “Next” button

![picture](images/jenkins3.png)

# Add AWS Credentials to Jenkins

> 1. From the home screen, hit the Credentials link in the left-side bar.
> 2. Determine where you want to put your credentials. If unsure, go into the Global credentials. You may want to do some reading on credential management for a production/widespread use.
> 3. Click the Add Credentials link in the left-side navigation.
> 4. For Kind, select AWS Credentials.
> 5. Enter the Access ID and Secret Access Key for the AWS user that has access to the ECR repository.
> 6. In the Advanced button, specify an ID that will make sense to you (aws_id).

![picture](images/aws.png)

# Add Bitbucket Credentials to Jenkins

> 1. From the home screen, hit the Credentials link in the left-side bar.
> 2. Determine where you want to put your credentials. If unsure, go into the Global credentials. You may want to do some reading on credential management for a production/widespread use.
> 3. Click the Add Credentials link in the left-side navigation.
> 4. For Kind, select Username and password.
> 5. Enter the username and password to login bitbucket (devopsnashtech@outlook.com/D3v0ps@P@ss)
> 6. In ID, specify an ID that will make sense to you (devopsnashtech)

![picture](images/bitbucket.png)

# Creating the Jenkins Pipeline

> 1. Create a new Jenkins job, of type “Pipeline”

![picture](images/pineline.png)

> 2. Config pineline

![picture](images/pineline1.png)

> 3. Run Build

![picture](images/build.png)
